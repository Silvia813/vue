import Vue from 'vue'
import App from './App.vue'
import HtmlDemo from './components/HtmlDemo'


// Vue.config.productionTip = true

const routes = {
  '/': App,
  '/htmldemo': HtmlDemo,
}
new Vue({
  el: '#app',
  data: {
    currentRoute: window.location.pathname
  },
  computed: {
    ViewComponent () {
      return routes[this.currentRoute]
    }
  },
  render (createElement) {
    return createElement(this.ViewComponent)
  }
})